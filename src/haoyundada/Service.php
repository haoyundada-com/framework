<?php
// +----------------------------------------------------------------------
// | Haoyundada for Wordpress framework
// +----------------------------------------------------------------------
// | Copyright (c) 2024~2024 http://haoyundada.com All rights reserved.
// +----------------------------------------------------------------------
// | @Remind: 使用盗版主题会存在各种未知风险。支持正版，从我做起！
// +----------------------------------------------------------------------
// | Author: THX <53297668@qq.com>
// +----------------------------------------------------------------------
declare(strict_types=1);

namespace haoyundada;

use Closure;
use haoyundada\event\RouteLoaded;

/**
 * 系统服务基础类
 * @method void register()
 * @method void boot()
 */
abstract class Service
{
    public function __construct(protected App $app)
    {
    }

    /**
     * 加载路由
     * @access protected
     * @param string $path 路由路径
     */
    protected function loadRoutesFrom(string $path)
    {
        $this->registerRoutes(function () use ($path) {
            include $path;
        });
    }

    /**
     * 注册路由
     * @param Closure $closure
     */
    protected function registerRoutes(Closure $closure)
    {
        $this->app->event->listen(RouteLoaded::class, $closure);
    }

    /**
     * 添加指令
     * @access protected
     * @param array|string $commands 指令
     */
    protected function commands($commands)
    {
        $commands = is_array($commands) ? $commands : func_get_args();

        Console::starting(function (Console $console) use ($commands) {
            $console->addCommands($commands);
        });
    }
}
