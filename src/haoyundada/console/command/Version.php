<?php
// +----------------------------------------------------------------------
// | haoyundadaPHP [ WE CAN DO IT JUST haoyundada IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006-2016 http://haoyundadaphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------
declare (strict_types = 1);

namespace haoyundada\console\command;

use Composer\InstalledVersions;
use haoyundada\console\Command;
use haoyundada\console\Input;
use haoyundada\console\Output;

class Version extends Command
{
    protected function configure()
    {
        // 指令配置
        $this->setName('version')
            ->setDescription('show haoyundadaphp framework version');
    }

    protected function execute(Input $input, Output $output)
    {
        $version = InstalledVersions::getPrettyVersion('tophaoyundada/framework');
        $output->writeln($version);
    }

}
