<?php
// +----------------------------------------------------------------------
// | Haoyundada for Wordpress framework
// +----------------------------------------------------------------------
// | Copyright (c) 2024~2024 http://haoyundada.com All rights reserved.
// +----------------------------------------------------------------------
// | @Remind: 使用盗版主题会存在各种未知风险。支持正版，从我做起！
// +----------------------------------------------------------------------
// | Author: THX <53297668@qq.com>
// +----------------------------------------------------------------------
declare(strict_types = 1);

namespace haoyundada\contract;

use DateInterval;
use DateTimeInterface;
use Psr\SimpleCache\CacheInterface;
use haoyundada\cache\TagSet;

/**
 * 缓存驱动接口
 */
interface CacheHandlerInterface extends CacheInterface
{

    /**
     * 自增缓存（针对数值缓存）
     * @param string $name 缓存变量名
     * @param int    $step 步长
     * @return false|int
     */
    public function inc($name, $step = 1);

    /**
     * 自减缓存（针对数值缓存）
     * @param string $name 缓存变量名
     * @param int    $step 步长
     * @return false|int
     */
    public function dec($name, $step = 1);

    /**
     * 读取缓存并删除
     * @param string $name 缓存变量名
     * @return mixed
     */
    public function pull($name);

    /**
     * 如果不存在则写入缓存
     * @param string                             $name   缓存变量名
     * @param mixed                              $value  存储数据
     * @param int|DateInterval|DateTimeInterface $expire 有效时间 0为永久
     * @return mixed
     */
    public function remember($name, $value, $expire = null);

    /**
     * 缓存标签
     * @param string|array $name 标签名
     * @return TagSet
     */
    public function tag($name);

    /**
     * 删除缓存标签
     * @param array $keys 缓存标识列表
     * @return void
     */
    public function clearTag($keys);
}
