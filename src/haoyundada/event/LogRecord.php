<?php
// +----------------------------------------------------------------------
// | Haoyundada for Wordpress framework
// +----------------------------------------------------------------------
// | Copyright (c) 2024~2024 http://haoyundada.com All rights reserved.
// +----------------------------------------------------------------------
// | @Remind: 使用盗版主题会存在各种未知风险。支持正版，从我做起！
// +----------------------------------------------------------------------
// | Author: THX <53297668@qq.com>
// +----------------------------------------------------------------------
namespace haoyundada\event;

/**
 * LogRecord事件类
 */
class LogRecord
{
    /** @var string */
    public $type;

    /** @var string */
    public $message;

    public function __construct($type, $message)
    {
        $this->type    = $type;
        $this->message = $message;
    }
}
