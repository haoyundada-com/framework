<?php
// +----------------------------------------------------------------------
// | haoyundadaPHP [ WE CAN DO IT JUST haoyundada IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006-2021 http://haoyundadaphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: yunwuxin <448901948@qq.com>
// +----------------------------------------------------------------------
declare (strict_types = 1);

namespace haoyundada\exception;

use haoyundada\Response;

/**
 * HTTP响应异常
 */
class HttpResponseException extends \RuntimeException
{
    public function __construct(protected Response $response)
    {
    }

    public function getResponse()
    {
        return $this->response;
    }

}
