<?php
// +----------------------------------------------------------------------
// | Haoyundada for Wordpress framework
// +----------------------------------------------------------------------
// | Copyright (c) 2024~2024 http://haoyundada.com All rights reserved.
// +----------------------------------------------------------------------
// | @Remind: 使用盗版主题会存在各种未知风险。支持正版，从我做起！
// +----------------------------------------------------------------------
// | Author: THX <53297668@qq.com>
// +----------------------------------------------------------------------
declare(strict_types=1);

namespace haoyundada\facade;

use haoyundada\Facade;

/**
 * @see \haoyundada\Middleware
 * @package haoyundada\facade
 * @mixin \haoyundada\Middleware
 * @method static void import(array $middlewares = [], string $type = 'global') 导入中间件
 * @method static void add(mixed $middleware, string $type = 'global') 注册中间件
 * @method static void route(mixed $middleware) 注册路由中间件
 * @method static void controller(mixed $middleware) 注册控制器中间件
 * @method static mixed unshift(mixed $middleware, string $type = 'global') 注册中间件到开始位置
 * @method static array all(string $type = 'global') 获取注册的中间件
 * @method static Pipeline pipeline(string $type = 'global') 调度管道
 * @method static mixed end(\haoyundada\Response $response) 结束调度
 * @method static \haoyundada\Response handleException(\haoyundada\Request $passable, \Throwable $e) 异常处理
 */
class Middleware extends Facade
{
    /**
     * 获取当前Facade对应类名（或者已经绑定的容器对象标识）
     * @access protected
     * @return string
     */
    protected static function getFacadeClass()
    {
        return 'middleware';
    }
}
