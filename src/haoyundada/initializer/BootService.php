<?php
// +----------------------------------------------------------------------
// | Haoyundada for Wordpress framework
// +----------------------------------------------------------------------
// | Copyright (c) 2024~2024 http://haoyundada.com All rights reserved.
// +----------------------------------------------------------------------
// | @Remind: 使用盗版主题会存在各种未知风险。支持正版，从我做起！
// +----------------------------------------------------------------------
// | Author: THX <53297668@qq.com>
// +----------------------------------------------------------------------
declare(strict_types=1);

namespace haoyundada\initializer;

use haoyundada\App;

/**
 * 启动系统服务
 */
class BootService
{
    public function init(App $app)
    {
        $app->boot();
    }
}
