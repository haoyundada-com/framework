<?php
// +----------------------------------------------------------------------
// | Haoyundada for Wordpress framework
// +----------------------------------------------------------------------
// | Copyright (c) 2024~2024 http://haoyundada.com All rights reserved.
// +----------------------------------------------------------------------
// | @Remind: 使用盗版主题会存在各种未知风险。支持正版，从我做起！
// +----------------------------------------------------------------------
// | Author: THX <53297668@qq.com>
// +----------------------------------------------------------------------
declare (strict_types = 1);

namespace haoyundada\route;

use haoyundada\Route;

/**
 * 域名路由
 */
class Domain extends RuleGroup
{
    /**
     * 架构函数
     * @access public
     * @param  Route       $router   路由对象
     * @param  string      $name     路由域名
     * @param  mixed       $rule     域名路由
     * @param  bool        $lazy   延迟解析
     */
    public function __construct(Route $router, ?string $name = null, $rule = null, bool $lazy = false)
    {
        $this->router = $router;
        $this->domain = $name;
        $this->rule   = $rule;

        if (!$lazy && !is_null($rule)) {
            $this->parseGroupRule($rule);
        }
    }

}
