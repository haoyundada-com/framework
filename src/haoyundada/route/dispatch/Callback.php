<?php
// +----------------------------------------------------------------------
// | Haoyundada for Wordpress framework
// +----------------------------------------------------------------------
// | Copyright (c) 2024~2024 http://haoyundada.com All rights reserved.
// +----------------------------------------------------------------------
// | @Remind: 使用盗版主题会存在各种未知风险。支持正版，从我做起！
// +----------------------------------------------------------------------
// | Author: THX <53297668@qq.com>
// +----------------------------------------------------------------------
declare (strict_types = 1);

namespace haoyundada\route\dispatch;

use haoyundada\exception\ClassNotFoundException;
use haoyundada\route\Dispatch;

/**
 * Callback Dispatcher
 */
class Callback extends Dispatch
{
    public function exec()
    {
        // 执行回调方法
        if (is_array($this->dispatch)) {
            [$class, $action] = $this->dispatch;

            // 设置当前请求的控制器、操作
            $controllerLayer = $this->rule->config('controller_layer') ?: 'controller';
            if (str_contains($class, '\\' . $controllerLayer . '\\')) {
                [$layer, $controller] = explode('/' . $controllerLayer . '/', trim(str_replace('\\', '/', $class), '/'));
                $layer                = trim(str_replace('app', '', $layer), '/');
            } else {
                $layer      = '';
                $controller = trim(str_replace('\\', '/', $class), '/');
            }

            if ($layer && !empty($this->option['auto_middleware'])) {
                // 自动为顶层layer注册中间件
                $alias = $this->app->config->get('middleware.alias', []);

                if (isset($alias[$layer])) {
                    $this->app->middleware->add($layer, 'route');
                }
            }

            $this->request
                ->setLayer($layer)
                ->setController($controller)
                ->setAction($action);

            if (class_exists($class)) {
                $instance = $this->app->invokeClass($class);
            } else {
                throw new ClassNotFoundException('class not exists:' . $class, $class);
            }

            return $this->responseWithMiddlewarePipeline($instance, $action);
        }

        $vars = $this->getActionBindVars();
        return $this->app->invoke($this->dispatch, $vars);
    }
}
