<?php
// +----------------------------------------------------------------------
// | Haoyundada for Wordpress framework
// +----------------------------------------------------------------------
// | Copyright (c) 2024~2024 http://haoyundada.com All rights reserved.
// +----------------------------------------------------------------------
// | @Remind: 使用盗版主题会存在各种未知风险。支持正版，从我做起！
// +----------------------------------------------------------------------
// | Author: THX <53297668@qq.com>
// +----------------------------------------------------------------------
namespace haoyundada\session\driver;

use Psr\SimpleCache\CacheInterface;
use haoyundada\contract\SessionHandlerInterface;
use haoyundada\helper\Arr;

class Cache implements SessionHandlerInterface
{

    /** @var CacheInterface */
    protected $handler;

    /** @var integer */
    protected $expire;

    /** @var string */
    protected $prefix;

    public function __construct(\haoyundada\Cache $cache, array $config = [])
    {
        $this->handler = $cache->store(Arr::get($config, 'store'));
        $this->expire  = Arr::get($config, 'expire', 1440);
        $this->prefix  = Arr::get($config, 'prefix', '');
    }

    public function read(string $sessionId): string
    {
        return (string) $this->handler->get($this->prefix . $sessionId);
    }

    public function delete(string $sessionId): bool
    {
        return $this->handler->delete($this->prefix . $sessionId);
    }

    public function write(string $sessionId, string $data): bool
    {
        return $this->handler->set($this->prefix . $sessionId, $data, $this->expire);
    }
}
